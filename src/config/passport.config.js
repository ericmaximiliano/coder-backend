import passport from "passport"; // SOLO RESPONSABLE DE DEVOLVER EL USUARIO
import local from "passport-local";
import GithubStrategy from "passport-github2";

import userManager from "../dao/mongo/Managers/user.js";
import cartManager from "../dao/mongo/Managers/cart.js";
import { createHash, validatePassword } from "../utils.js";


const user = new userManager;
const cart = new cartManager;

const LocalStrategy = local.Strategy; // username + password

export const initializePassport = () => {
    passport.use('register', new LocalStrategy({ passReqToCallback: true, usernameField: 'email' }, async (req, email, password, done) => {
        //Agregar validaciones de datos recibidos 
        try {
            const { first_name, last_name } = req.body

            const userExists = await user.getUser(email);

            if (userExists) return done(null, false, { message: "User alredy exists" }) // null para no crashear app - false para indicar que no hay usuario encontrado - {} para enviar opciones

            const hashedPassword = await createHash(password);

            const newCart = await cart.createCart()

            const newUser = {
                first_name,
                last_name,
                email,
                password: hashedPassword,
                cart: newCart._id
            }

            console.log(newUser)

            const result = await user.createUser(newUser);
            if (!result) return done(null, false, { message: "Error creating user" })

            done(null, result); // por el caso OK: null para no enviar un error - result para enviar el obj creado en la db

        } catch (error) {
            done(error);
        }
    }))

    passport.use('login', new LocalStrategy({ usernameField: 'email' }, async (email, password, done) => {
        //Agregar validaciones de datos recibidos 
        try {
            /// logica para usuario admin
            if (email === "adminCoder@coder.com" && password === "adminCod3r123") {
                const userData = {
                    id: 999,
                    first_name: "Admin",
                    last_name: "Coderhouse",
                    role: "admin"
                };
                return done(null, userData);
            }
            /// logica para usuarios normales
            const userExists = await user.getUser(email);
            if (userExists) {
                const checkPassword = await validatePassword(password, userExists.password);
                if (!checkPassword) return done(null, false, { message: "Incorrect password" })

                const userData = {
                    id: userExists._id,
                    first_name: `${userExists.first_name}`,
                    last_name: `${userExists.last_name}`,
                    email: userExists.email,
                    role: userExists.role,
                    cart: userExists.cart
                };

                done(null, userData); // por el caso OK: null para no enviar un error - result para enviar el obj encontrado en mongo
            } else {
                return done(null, false, { message: "User does not exist" })
            }
        } catch (error) {
            done(error);
        }
    }))

    passport.serializeUser(function (userData, done) {
        return done(null, userData.id)
    });

    passport.deserializeUser(async function (id, done) {
        if (id === 999) {
            return done(null, {
                first_name: "Admin",
                last_name: "Coderhouse",
                role: "admin"
            })
        }
        const user = await user.getUserById(id);
        return done(null, user)
    });
};

export const initializePassportGithub = () => {
    // Logica similar al /register
    passport.use('github', new GithubStrategy({
        clientID: "Iv1.79f1c36390ccc009",
        clientSecret: "0204f01d175d71f5270753a62d994a55e9654f34",
        callbackURL: "http://localhost:8080/api/sessions/githubcallback"
    }, async (accessToken, refreshToken, profile, done) => {
        try {
            const { name, email } = profile._json
            // busco en mi db por mail y si existe, salgo directamente
            const userExists = await user.getUser(email);
            if (userExists) return done(null, userExists)
            // si no existe lo registro y luego en el callback lo llevo a la vista de products
            if (!userExists) {
                // le asigno password 123 por defecto
                const passwordDefault = "123"
                const hashedPassword = await createHash(passwordDefault);
                const newCart = await cart.createCart()
                const newUser = {
                    first_name: name,
                    email,
                    password: hashedPassword,
                    cart: newCart._id
                }
                const result = await user.createUser(newUser);
                if (!result) return done(null, false, { message: "Error creating user" })
                // si sale todo ok                
                done(null, result)
            }
        } catch (error) {
            done(error)
        }
    }))
}

export default {
    initializePassport,
    initializePassportGithub
};