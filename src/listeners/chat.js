import MessagesManager from "../dao/mongo/Managers/chat.js";

const messageManager = new MessagesManager();

const registerChat = (io, socket) => {

    const saveMessage = async (message) => {
        await messageManager.createMessage(message)
        const messageLogs = await messageManager.getMessages();
        io.emit('chat:messageLogs', messageLogs)
    } // similar a pushear y luego se recuperan los msg 

    const newParticipant = (user) => {
        socket.broadcast.emit('chat:newConnection')
    }

    socket.on('chat:message', saveMessage);
    socket.on('chat:newParticipant', newParticipant)
}

export default registerChat;