// esta vista SI utiliza la conexion por socket y actualiza en tiempo real los productos segun se agreguen o eliminen
// a traves del endpoint del router de products
console.log("realtimeproducts.js conectado")

const socket = io();

socket.on('products', resultProducts => {
    const allProductsContent = document.getElementById('productContent')
    let content = ""
    resultProducts.forEach(products => {
        content += `Producto nro: ${products.id}. Titulo: ${products.title}. </br>`
    });
    allProductsContent.innerHTML = content;
})