const form = document.getElementById('logoutButton');

form.addEventListener('click', async (event) => {
    event.preventDefault();

    const response = await fetch('/api/sessions/logout', {
        method: 'GET',

        headers: {
            'Content-Type': 'application/json',
        },
    });

    if (response.status === 200) {
        window.location.replace('/login');
    }
});