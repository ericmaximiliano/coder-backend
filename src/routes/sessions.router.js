import { Router } from "express";
import userManager from "../dao/mongo/Managers/user.js";
import passport from "passport";
import { createHash, validatePassword } from "../utils.js";

const router = Router();

const user = new userManager;

router.post('/register', passport.authenticate('register', { failureRedirect: '/api/sessions/registerFail' }), async (req, res) => {
    res.status(200).redirect('/login'); // si cree ok el user directamente lo llevo para logearse
})

router.get('/registerFail', (req, res) => {
    return res.status(400).send({ status: "error", error: req.session.message });
})

router.post('/login', passport.authenticate('login', { failureRedirect: '/api/sessions/loginFail' }), async (req, res) => {
    const result = req.user
    // si viene last_name en undefined quiere decir que el alta se hizo por github y como no parsee
    // el profile._json queda el nombre completo en first_name. caso contrario el alta fue por formulario de register
    if (result.last_name === 'undefined') {
        req.session.user = {
            name: `${result.first_name}`,
            email: result.email
        }
    } else {
        req.session.user = {
            name: `${result.first_name} ${result.last_name}`,
            email: result.email
        }
    }

    const usuarioLogueado = req.session.user;
    console.log(usuarioLogueado)
    res.status(200).redirect('/products');
})

router.get('/loginFail', (req, res) => {
    return res.status(400).send({ status: "error", error: req.session.message });
})

router.get('/logout', async (req, res) => {
    res.clearCookie('connect.sid')
    req.session.destroy();
    res.sendStatus(200);
});

router.get('/github', passport.authenticate('github'), async (req, res) => {

})

router.get('/githubcallback', passport.authenticate('github'), (req, res) => {
    const result = req.user

    req.session.user = {
        name: `${result.first_name}`, // contiene first_name + last_name en un mismo campo
        email: result.email
    }

    res.status(200).redirect('/products');
})

router.get('/current', (req, res) => {
    // Verifica si hay un usuario logueado en la sesión
    if (req.session.user) {
        // Accede a la información del usuario logueado
        const usuarioLogueado = req.session.user;
        res.send(`Usuario logueado: ${usuarioLogueado.name}. Direccion de correo registrado: ${usuarioLogueado.email}`);
    } else {
        res.send('No hay usuario logueado');
    }
});

export default router;