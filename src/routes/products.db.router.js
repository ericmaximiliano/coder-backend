// rutas para acceder a la integracion con base de datos MongoDB a traves de moongose
import { Router } from "express";
import ProductManager from "../dao/mongo/Managers/products.js";
const router = Router();

const productManager = new ProductManager();

router.get('/', async (req, res) => {
    let valueLimit = 10 //valor por defecto
    let valuePage = 1 //valor por defecto
    let valueSort = '' //valor por defecto
    // let valueCategory = '' //valor por defecto
    // let valueStatus = '' //valor por defecto
    const key = Object.keys(req.query)
    // recorro el array con todos las keys enviadas y en caso de ser validas
    // guardo su indice para luego saber de donde obtener el valor del parametro
    // y poder aplicarlos a los filtros de paginate
    key.forEach((element, index) => {
        switch (element) {
            case 'limit':
                valueLimit = parseInt(Object.values(req.query)[index])
                break;
            case 'page':
                valuePage = parseInt(Object.values(req.query)[index])
                break;
            case 'sort':
                valueSort = Object.values(req.query)[index]
                if (valueSort === "asc" || valueSort === "1") {
                    valueSort = 1
                    break;
                }
                if (valueSort === "desc" || valueSort === "-1") {
                    valueSort = -1
                    break;
                }
            // case 'category':
            //     valueCategory = Object.values(req.query)[index]
            //     break;
            // case 'status':
            //     valueStatus = Object.values(req.query)[index]
            //     if (valueStatus === "true" || valueStatus === "1") {
            //         valueStatus = true
            //         break;
            //     }
            //     if (valueStatus === "false" || valueStatus === "-1") {
            //         valueStatus = false
            //         break;
            //     }
            //     break;
        }
    });

    const params = {
        limit: Number(valueLimit),
        page: Number(valuePage),
        sort: { price: Number(valueSort) }
    }

    if (params.sort.price != 1 && params.sort.price != -1) {
        delete params.sort.price
        delete params.sort
    }

    const products = await productManager.getProducts(params);
    if (!products) return res.send({ status: 'error', payload: products });

    if (products.hasPrevPage) {
        products.prevLink = `/api/products/?page=${products.prevPage}`
    } else {
        products.prevLink = null
    }
    if (products.hasNextPage) {
        products.nextLink = `/api/products/?page=${products.nextPage}`
    } else {
        products.nextLink = null
    }

    res.send({ status: 'success', payload: products });

});


router.post("/", async (req, res) => {

    const newProduct = {
        title: req.body.title,
        description: req.body.description,
        code: req.body.code,
        price: req.body.price,
        status: req.body.status,
        stock: req.body.stock,
        category: req.body.category,
        thumbnail: req.body.thumbnail
    }

    // if (!title || !description || !code || !price || !status || !category || !thumbnail)
    //     return res.status(400).send({ status: "error", error: "Incomplete Values" });

    const result = await productManager.createProduct(newProduct);
    if (!result) return res.status(500).send({ status: "error", error: "Backend failure" });
    res.sendStatus(201);

});

router.get('/:pid', async (req, res) => {
    const { pid } = req.params
    const product = await productManager.getProductById(pid)
    if (!product) return res.status(500).send({ status: "error", error: "Backend failure" });
    res.send({ status: 'success', payload: product });
})

router.put('/:pid', async (req, res) => {
    const { pid } = req.params
    const updateProduct = {
        title: req.body.title,
        description: req.body.description,
        code: req.body.code,
        price: req.body.price,
        status: req.body.status,
        stock: req.body.stock,
        category: req.body.category,
        thumbnail: req.body.thumbnail
    }
    const result = await productManager.updateProduct(pid, updateProduct)
    if (!result) return res.status(500).send({ status: "error", error: "Backend failure" });
    res.status(200).send({ status: "success", message: "Updated product" });
})


router.delete('/:pid', async (req, res) => {
    const { pid } = req.params
    const result = await productManager.deleteProduct(pid)
    if (!result) return res.status(500).send({ status: "error", error: "Backend failure" });
    res.status(200).send({ status: "success", message: "Product deleted" });

})


export default router;
