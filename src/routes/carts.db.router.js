// rutas para acceder a la integracion con sistemas de archivos locales
import { Router } from 'express';
import CartManager from "../dao/mongo/Managers/cart.js";
const router = Router();

// creo instancia del cart
const cart = new CartManager()

// crea un nuevo carrito vacio
router.post('/', async (req, res) => {
    const result = await cart.createCart()
    if (!result) return res.status(500).send({ status: "error", error: "Backend failure" });
    res.status(200).send({ status: 'success', message: "New cart created" });
})

// buscar todos los carritos
router.get('/', async (req, res) => {
    const carts = await cart.getCarts()
    if (!carts) return res.status(500).send({ status: "error", error: "Backend failure" });
    res.send({ status: 'success', payload: carts });
})

// buscar un carrito por el id
router.get('/:cid', async (req, res) => {
    const { cid } = req.params
    const carts = await cart.getCartById(cid)
    if (!carts) return res.status(500).send({ status: "error", error: "Backend failure" });
    res.send({ status: 'success', payload: carts });
})

// agregar un producto al carrito
router.post('/:cid/product/:pid', async (req, res) => {
    const { cid, pid } = req.params
    const quantity = parseInt(req.body.quantity) || 1;
    const newProduct = await cart.addProductInCart(cid, pid, quantity)
    if (!newProduct) return res.status(500).send({ status: "error", error: "Backend failure" });
    res.status(200).send({ status: 'success', message: "Product added to cart" });
})

//actualiza el carrito
router.put('/:cid', async (req, res) => {
    const { cid, } = req.params
    const { products } = req.body
    // agregar validacion para controlar que venga el id_product y la quantity siempre
    const result = await cart.addProductsInCart(cid, products)
    console.log(result)
    if (!result) return res.status(500).send({ status: "error", error: "Backend failure" });
    res.status(200).send({ status: 'success', message: "Product added to cart" });
})

//actualiza el la quantity de un producto dentro de un carrito
router.put('/:cid/product/:pid', async (req, res) => {
    const { cid, pid } = req.params
    const quantity = parseInt(req.body.quantity)
    const result = await cart.updateQuantityCart(cid, pid, quantity)
    if (!result) return res.status(500).send({ status: "error", error: "Backend failure" });
    res.status(200).send({ status: 'success', message: "Quantity updated" });
})


//vaciar un carrito
router.delete('/:cid', async (req, res) => {
    const { cid } = req.params
    const params = { products: [] }
    const result = await cart.deleteCart(cid, params)
    if (!result) return res.status(500).send({ status: "error", error: "Backend failure" });
    res.status(200).send({ status: 'success', message: "Cart emptied" });
})

//eliminar un producto del carrito
router.delete('/:cid/products/:pid', async (req, res) => {
    const { cid, pid } = req.params
    const result = await cart.deleteProductCart(cid, pid)
    if (!result) return res.status(500).send({ status: "error", error: "Backend failure" });
    res.status(200).send({ status: 'success', message: "Product removed from cart" });
})

export default router;