import { Router } from "express";
import { privacy } from "../middlewares/auth.js";
import ProductManager from "../dao/mongo/Managers/products.js";
import CartManager from "../dao/mongo/Managers/cart.js";
const router = Router();

// creo instancias
const productManager = new ProductManager();
const cart = new CartManager()

router.get('/chat', async (req, res) => {
    res.render('chat');
})

router.get('/products', async (req, res) => {
    let valueLimit = 10 //valor por defecto
    let valuePage = 1 //valor por defecto
    let valueSort = '' //valor por defecto
    const key = Object.keys(req.query)
    // recorro el array con todos las keys enviadas y en caso de ser validas
    // guardo su indice para luego saber de donde obtener el valor del parametro
    // y poder aplicarlos a los filtros de paginate
    key.forEach((element, index) => {
        switch (element) {
            case 'limit':
                valueLimit = parseInt(Object.values(req.query)[index])
                break;
            case 'page':
                valuePage = parseInt(Object.values(req.query)[index])
                break;
            case 'sort':
                valueSort = Object.values(req.query)[index]
                if (valueSort === "asc" || valueSort === "1") {
                    valueSort = 1
                    break;
                }
                if (valueSort === "desc" || valueSort === "-1") {
                    valueSort = -1
                    break;
                }
        }
    });


    const params = {
        limit: Number(valueLimit),
        page: Number(valuePage),
        sort: { price: Number(valueSort) }
    }

    if (params.sort.price != 1 && params.sort.price != -1) {
        delete params.sort.price
        delete params.sort
    }

    const { docs, hasPrevPage, hasNextPage, prevPage, nextPage } = await productManager.getProducts(params);

    const products = docs

    if (!products) return res.send({ status: 'error', payload: products });

    res.render('products', { products, valuePage, valueLimit, valueSort, hasPrevPage, hasNextPage, prevPage, nextPage, user: req.session.user });

});

router.get('/carts/:cid', async (req, res) => {
    // id cart test: 646fe63e77c3146a4617c3ff con 2 products
    const { cid } = req.params
    const cartProducts = await cart.getCartById(cid);
    console.log(cartProducts)
    res.render('cart', { cartProducts });
})

router.get('/register', async (req, res) => {
    res.render('register');
})

router.get('/login', async (req, res) => {
    res.render('login');
})


export default router;