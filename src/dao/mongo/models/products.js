import mongoose from 'mongoose';
import moongosePaginate from 'mongoose-paginate-v2';

const collection = 'products';

const schema = new mongoose.Schema(
    {
        title: String,
        description: String,
        code: String,
        price: Number,
        status: Boolean,
        stock: Number,
        category: String,
        thumbnail: []
    },
    { timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' } }
);
schema.plugin(moongosePaginate);
const productModel = mongoose.model(collection, schema);

export default productModel;
