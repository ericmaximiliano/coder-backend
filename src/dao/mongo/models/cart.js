import mongoose from 'mongoose';

const collection = 'carts';

const schema = new mongoose.Schema(
    {
        products: {
            type: [
                {
                    id_product: {
                        type: mongoose.SchemaTypes.ObjectId,
                        ref: 'products'
                    },
                    quantity: Number
                }
            ]
        }
    },
    { timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' } }
);

schema.pre('find', function () {
    this.populate('products.id_product');
})
schema.pre('findOne', function () {
    this.populate('products.id_product');
})

const cartModel = mongoose.model(collection, schema);

export default cartModel;
