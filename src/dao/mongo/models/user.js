import mongoose from 'mongoose';

const collection = "users";

const schema = new mongoose.Schema({
    first_name: String,
    last_name: String,
    email: String,
    password: String,
    role: {
        type: String,
        default: "user"
    },
    cart: {
        type: mongoose.SchemaTypes.ObjectId,
        ref: 'carts'
    }
}, { timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' } }
)

const userModel = mongoose.model(collection, schema);

export default userModel;