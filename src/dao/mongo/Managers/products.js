import productModel from '../models/products.js';

export default class ProductManager {
    getProducts = (params) => {
        return productModel.paginate({}, { limit: params.limit, page: params.page, sort: params.sort, lean: true });
    };

    getProductById = (pid) => {
        return productModel.findOne({ _id: pid }).lean();
    };

    createProduct = (product) => {
        return productModel.create(product);
    };

    updateProduct = async (pid, product) => {
        return productModel.findByIdAndUpdate({ _id: pid }, { $set: product });
    };

    deleteProduct = async (pid) => {
        return productModel.findByIdAndDelete({ _id: pid });
    };
}