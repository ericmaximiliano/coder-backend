import cartModel from '../models/cart.js';
import mongoose from 'mongoose';


export default class cartManager {

    createCart = (cart) => {
        return cartModel.create(cart);
    };

    getCarts = (params) => {
        return cartModel.find(params);
    };

    getCartById = (cid) => {
        return cartModel.findOne({ _id: cid }).lean();
    };

    addProductInCart = async (cid, pid, quantity) => {
        return cartModel.updateOne({ _id: cid },
            {
                $push: {
                    products: { id_product: new mongoose.Types.ObjectId(pid), quantity: quantity },
                }
            });
    };

    addProductsInCart = async (cid, products) => {
        // body para test
        // {
        //     "products":[
        //         {
        //             "id_product": "646f14484f0651a17f3039ed",
        //             "quantity": 21
        //         },
        //         {
        //             "id_product": "646f14564f0651a17f3039f1",
        //             "quantity": 25
        //         }        
        //     ]
        // }

        try {
            const update = {
                $push: { products: { $each: products } }
            };
            return cartModel.updateOne({ _id: cid }, update);
        } catch (error) {
            console.log(error)
        }
    }

    deleteCart = (cid, params) => {
        return cartModel.findByIdAndUpdate({ _id: cid }, { $set: params });
    };

    deleteProductCart = (cid, pid) => {
        return cartModel.updateOne({ _id: cid },
            {
                $pull: { products: { id_product: pid }, }
            });
    };

    updateQuantityCart = (cid, pid, quantity) => {
        return cartModel.updateOne({ _id: cid, "products.id_product": pid }, { $set: { "products.$.quantity": quantity } });
    }

}