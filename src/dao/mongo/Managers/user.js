import userModel from '../models/user.js';
import mongoose from 'mongoose';

export default class userManager {
    createUser = (user) => {
        return userModel.create(user);
    };

    getUser = (email) => {
        return userModel.findOne({ email: email }).lean();
    };

    getUserById = (id) => {
        return userModel.findOne({ _id: id }).lean();
    };
}