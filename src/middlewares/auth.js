export const privacy = (privacyType) => {
    return (req, res, next) => {
        const { user } = req.session;
        switch (privacyType) {
            case "PRIVATE":
                if (user) next();
                else res.redirect('/login')
                break;
            case "NO_AUTH":
                if (!user) next()
                else res.direct('/products')
                break;

        }
    }
}