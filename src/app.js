// importacion de modulos externos
import express from 'express';
import session from 'express-session';
import handlebars from "express-handlebars"
import MongoStore from 'connect-mongo'
import mongoose from 'mongoose';
import passport from "passport";
import { Server } from 'socket.io';

// importacion de modulos internos
import { initializePassport, initializePassportGithub } from './config/passport.config.js';
import sessionsRouter from "./routes/sessions.router.js";
import productsDbRouter from "./routes/products.db.router.js";
import cartsDbRouter from "./routes/carts.db.router.js";
import viewsDbRouter from "./routes/views.db.router.js";
import registerChat from "./listeners/chat.js";
import __dirname from './utils.js';

const app = express(); // inicializacion de express
const PORT = process.env.PORT || 8080 // seteo un puerto cualquiera del ambiente o el 8080
const server = app.listen(PORT, () => { console.log(`Server started: ${PORT}`) })
const io = new Server(server); // server de sockets
const connection = mongoose.connect("mongodb+srv://coderhouse-backend:coder1234@coderhouse-backend.8esdaez.mongodb.net/ecommerce?retryWrites=true&w=majority") // comando para conectar con mongodb - incluye nombre cluster y password
app.use(express.json()); // permite leer JSON en peticiones
app.use(express.urlencoded({ extended: true })) // permite leer parametros por URL
app.use(express.static(`${__dirname}/public`)); // permite acceder a archivos estaticos
app.engine('handlebars', handlebars.engine()) // seteo motor de handlebars
app.set('views', `${__dirname}/views`); // seteo ubicacion por defecto de las vistas handlebars
app.set('view engine', 'handlebars');

app.use(session({
    store: new MongoStore({
        mongoUrl: "mongodb+srv://coderhouse-backend:coder1234@coderhouse-backend.8esdaez.mongodb.net/ecommerce?retryWrites=true&w=majority",
        ttl: 3600,
    }),
    secret: "3st03suns3cre7",
    resave: false,
    saveUninitialized: true
}))


// middleware para referenciar al req.io y poder usarlo en otros endpoint
app.use((req, res, next) => {
    req.io = io;
    next()
})

// inicializacion de passport
app.use(passport.initialize());
initializePassport();
initializePassportGithub();
// referencias a los routers con mongodb
app.use('/', viewsDbRouter); // router par administrar la logica de vistas (fronts de mis app) con mongo // ROUTER POR DEFECTO AL INGRESAR A LA URL BASE
app.use('/api/products', productsDbRouter); // router par administrar la logica de productos con mongo
app.use('/api/carts', cartsDbRouter); // router par administrar la logica de carritos con mongo
app.use('/api/sessions', sessionsRouter); // router par administrar la logica de carritos con mongo

// ON escucha eventos
io.on('connection', socket => {
    registerChat(io, socket);
    console.log(`New socket connected. ID: ${socket.id}`)
})